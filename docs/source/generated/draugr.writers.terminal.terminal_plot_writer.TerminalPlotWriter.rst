draugr.writers.terminal.terminal\_plot\_writer.TerminalPlotWriter
=================================================================

.. currentmodule:: draugr.writers.terminal.terminal_plot_writer

.. autoclass:: TerminalPlotWriter
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~TerminalPlotWriter.__init__
      ~TerminalPlotWriter.blip
      ~TerminalPlotWriter.close
      ~TerminalPlotWriter.filter
      ~TerminalPlotWriter.image
      ~TerminalPlotWriter.open
      ~TerminalPlotWriter.scalar
      ~TerminalPlotWriter.stats
   
   

   
   
   