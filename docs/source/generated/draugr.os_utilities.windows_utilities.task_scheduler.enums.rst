draugr.os\_utilities.windows\_utilities.task\_scheduler.enums
=============================================================

.. automodule:: draugr.os_utilities.windows_utilities.task_scheduler.enums

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom_autosummary/module.rst
   :recursive:

   draugr.os_utilities.windows_utilities.task_scheduler.enums.task_action_type
   draugr.os_utilities.windows_utilities.task_scheduler.enums.task_creation
   draugr.os_utilities.windows_utilities.task_scheduler.enums.task_logon_type
   draugr.os_utilities.windows_utilities.task_scheduler.enums.task_trigger

