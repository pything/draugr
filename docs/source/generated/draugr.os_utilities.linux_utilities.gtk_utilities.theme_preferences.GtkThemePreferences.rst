﻿draugr.os\_utilities.linux\_utilities.gtk\_utilities.theme\_preferences.GtkThemePreferences
===========================================================================================

.. currentmodule:: draugr.os_utilities.linux_utilities.gtk_utilities.theme_preferences

.. autoclass:: GtkThemePreferences
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   