draugr.os\_utilities.windows\_utilities.win32
=============================================

.. automodule:: draugr.os_utilities.windows_utilities.win32

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom_autosummary/module.rst
   :recursive:

   draugr.os_utilities.windows_utilities.win32.api
   draugr.os_utilities.windows_utilities.win32.gui

