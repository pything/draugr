draugr.os\_utilities.windows\_utilities.win32.gui
=================================================

.. automodule:: draugr.os_utilities.windows_utilities.win32.gui

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      list_window_names
      get_inner_windows
      find_all_windows
   
   

   
   
   

   
   
   



