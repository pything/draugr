﻿draugr.os\_utilities.linux\_utilities.gtk\_utilities.gtk\_settings.GtkSettings
==============================================================================

.. currentmodule:: draugr.os_utilities.linux_utilities.gtk_utilities.gtk_settings

.. autoclass:: GtkSettings
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   