draugr.os\_utilities.windows\_utilities.task\_scheduler.enums.task\_action\_type
================================================================================

.. automodule:: draugr.os_utilities.windows_utilities.task_scheduler.enums.task_action_type

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
      :toctree:
      :template: custom_autosummary/class.rst
   
      TaskActionTypeEnum
   
   

   
   
   



