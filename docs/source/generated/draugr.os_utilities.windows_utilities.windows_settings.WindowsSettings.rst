draugr.os\_utilities.windows\_utilities.windows\_settings.WindowsSettings
=========================================================================

.. currentmodule:: draugr.os_utilities.windows_utilities.windows_settings

.. autoclass:: WindowsSettings
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~WindowsSettings.__init__
      ~WindowsSettings.get_dark_mode
      ~WindowsSettings.set_dark_mode
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~WindowsSettings.all_settings
      ~WindowsSettings.instance
   
   