draugr.os\_utilities.windows\_utilities
=======================================

.. automodule:: draugr.os_utilities.windows_utilities

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom_autosummary/module.rst
   :recursive:

   draugr.os_utilities.windows_utilities.exclude
   draugr.os_utilities.windows_utilities.task_scheduler
   draugr.os_utilities.windows_utilities.win32
   draugr.os_utilities.windows_utilities.windows_settings

