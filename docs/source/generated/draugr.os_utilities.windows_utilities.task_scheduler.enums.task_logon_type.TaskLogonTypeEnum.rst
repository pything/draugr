draugr.os\_utilities.windows\_utilities.task\_scheduler.enums.task\_logon\_type.TaskLogonTypeEnum
=================================================================================================

.. currentmodule:: draugr.os_utilities.windows_utilities.task_scheduler.enums.task_logon_type

.. autoclass:: TaskLogonTypeEnum
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TaskLogonTypeEnum.TASK_LOGON_NONE
      ~TaskLogonTypeEnum.TASK_LOGON_PASSWORD
      ~TaskLogonTypeEnum.TASK_LOGON_S4U
      ~TaskLogonTypeEnum.TASK_LOGON_INTERACTIVE_TOKEN
      ~TaskLogonTypeEnum.TASK_LOGON_GROUP
      ~TaskLogonTypeEnum.TASK_LOGON_SERVICE_ACCOUNT
      ~TaskLogonTypeEnum.TASK_LOGON_INTERACTIVE_TOKEN_OR_PASSWORD
   
   