draugr.os\_utilities.windows\_utilities.task\_scheduler.api.delete\_task
========================================================================

.. currentmodule:: draugr.os_utilities.windows_utilities.task_scheduler.api

.. autofunction:: delete_task