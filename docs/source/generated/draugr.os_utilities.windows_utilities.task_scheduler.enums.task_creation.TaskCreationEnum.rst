draugr.os\_utilities.windows\_utilities.task\_scheduler.enums.task\_creation.TaskCreationEnum
=============================================================================================

.. currentmodule:: draugr.os_utilities.windows_utilities.task_scheduler.enums.task_creation

.. autoclass:: TaskCreationEnum
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TaskCreationEnum.TASK_VALIDATE_ONLY
      ~TaskCreationEnum.TASK_CREATE
      ~TaskCreationEnum.TASK_UPDATE
      ~TaskCreationEnum.TASK_CREATE_OR_UPDATE
      ~TaskCreationEnum.TASK_DISABLE
      ~TaskCreationEnum.TASK_DONT_ADD_PRINCIPAL_ACE
      ~TaskCreationEnum.TASK_IGNORE_REGISTRATION_TRIGGERS
   
   