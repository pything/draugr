﻿draugr.os\_utilities.linux\_utilities.systemd\_utilities.service\_management.RunAsEnum
======================================================================================

.. currentmodule:: draugr.os_utilities.linux_utilities.systemd_utilities.service_management

.. autoclass:: RunAsEnum
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   